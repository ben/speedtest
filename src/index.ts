import Speedtest from "./Speedtest";

let interval: number = 1800000; // ms
let file: string = "./dist/data/data.xlsx";

run();
setInterval(() => run(), interval);

function run (): void {
	new Speedtest(file)
		.run();
}
