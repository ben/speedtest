# Speedtest Logger
### What is it?
This script runs an [internet speed test](https://speedtest.net/) at a certain interval, and saves the results to a file.

### Setup:

1. Clone this repo<br>`git clone https://gitdab.com/ben/speedtest.git`
2. Install the dependencies<br>`npm install`
3. Compile<br>`npx tsc`
4. Create a blank `xlsx` file
5. Set the interval and path in `dist/index.js`
6. Start the script*<br>`node dist/index.js`


\* I reccomend using something similar to [PM2](https://pm2.keymetrics.io/) to ensure the script is always running